# run:  python3 random_pdf.py N
# where N is the number of events generated

# imports------------------------
import numpy as np
from scipy.optimize import curve_fit
import random, time, sys
import matplotlib.pyplot as plt
import logging as logg

# initial settings-------------------
trials = sys.argv[1]
random.seed(1234)
logg.basicConfig(format='%(asctime)s\t%(levelname)s\t%(message)s',filename='exam.log',level=logg.INFO)

# function definitions-------------------
def pdf(t):		return np.exp(-t)
def cdf(t):		return 1.0-np.exp(-t)
def inverse_cdf(u):	return -np.log(1.0-u)
def fit_function(x, A, B, C):
	return A * np.exp(-B * x) + C

#a fast glance at the pdf and cdf
plt.plot([0,1,2,3,4],[pdf(0),pdf(1),pdf(2),pdf(3),pdf(4)])
plt.plot([0,1,2,3,4],[cdf(0),cdf(1),cdf(2),cdf(3),cdf(4)])
plt.show()


#----------------------------------------------------
# ========= HIT OR MISS METHOD ===========
xmin = 0
xmax = 5.
x = []
goodX = []

start = time.time()

for i in range(int(trials)):
	x.append(random.uniform(xmin, xmax))
	if random.uniform(0,1.) <= pdf(x[-1]):
		goodX.append(x[-1])

end = time.time()
logg.info("Hit or miss processing time: %f s. Events: %d" %((end-start),int(trials)))

plot_tuple = plt.hist(goodX,bins=20, density=True, range=(xmin,xmax))
plt.ylabel('exponential pdf (normalized)')
plt.xlabel('random variable x')
plt.show()

# fit--------
binCent = []
for i in range(len(plot_tuple[1])-1):
	binCent.append( (plot_tuple[1][i+1]+plot_tuple[1][i])/2. )

popt, pcov = curve_fit(fit_function, xdata=binCent, ydata=plot_tuple[0])
print("Fit parameters (hit or miss): ", popt)


#----------------------------------------------------
# ========= INVERSE TRANSFORM SAMPLING ===========
random.seed(4231)
x.clear()
goodX.clear()
del plot_tuple

start = time.time()

for i in range(int(trials)):
	u = random.uniform(0,1.0)
	x.append(-np.log(1.0-u))

end = time.time()
logg.info("Inverse transform sampling processing time: %f s. Events: %d\n" %((end-start),int(trials)))

plot_tuple = plt.hist(x,bins=20, density=True, range=(xmin, xmax))
plt.ylabel('exponential pdf (normalized)')
plt.xlabel('random variable x')
plt.show()

# fit--------
binCent.clear()

for i in range(len(plot_tuple[1])-1):
	binCent.append( (plot_tuple[1][i+1]+plot_tuple[1][i])/2. )

popt, pcov = curve_fit(fit_function, xdata=binCent, ydata=plot_tuple[0])
print("Fit parameters (inverse transform): ", popt)
